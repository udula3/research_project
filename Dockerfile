#alpine latest image
FROM python:3.7

#apk is the package mannager of the alpine image. i no need cache
#RUN apk add --no-cache python3-dev \
#   && pip3 install --upgrade pip 

#create dir in docker. like mkdir
ADD . ./app/
WORKDIR /app/
EXPOSE 5000
#then copy  . mean current dir to /app dir. take everything from current dir to working dir (app)
# COPY . /app 
RUN pip3 install --upgrade pip
RUN pip3 install -r reqs.txt

# ENTRYPOINT ["python3"]
CMD ["flask", "run", "--host", "0.0.0.0"]
# FROM python:3.7.1
# ADD reqs.txt reqs.txt
# RUN pip3 install -r reqs.txt


