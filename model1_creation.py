import numpy as np
import pandas as pd
import datetime
import time
import statsmodels.api as sm
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_squared_error
from statsmodels.tsa.ar_model import AutoRegResults
from sklearn.model_selection import train_test_split
from pylab import rcParams
from statsmodels.tsa.stattools import kpss
import itertools
import requests
from sklearn.externals import joblib
from app import app, lm

def send_build_status(model_id,progress,status):
    result = app.mongo.db.models.find_one({"id":model_id})
    if result == None:
      app.mongo.db.models.insert_one({"id":model_id,'progress':progress,'status':status})
    #   return jsonify('thread_name inserted')
    else:
      app.mongo.db.models.update({"id":model_id},{'$set':{'progress':progress,'status':status}})
    #   return jsonify('thread_name updated')
    # result = app.mongo.db.models.update({"id":id},{'$set':{'progress':progress,'status':status}})

def kpss_test(series, **kw):    
    statistic, p_value, n_lags, critical_values = kpss(series, **kw)
    # Format Output
    print(f'KPSS Statistic: {statistic}')
    print(f'p-value: {p_value}')
    print(f'num lags: {n_lags}')
    print('Critial Values:')
    for key, value in critical_values.items():
        print(f'   {key} : {value}')
    print(f'Result: The series is {"not " if p_value < 0.05 else ""}stationary')

def build_model():
    try:
        data=pd.read_csv("new_dataset.csv")
        send_build_status(1,10,'PENDING')
        # r = requests.post('http://localhost:5000/model/build/status', data = {'key':'value'})
        data
        temp=data['Date'].values
        temp2 = [datetime.datetime.strptime(x, "%Y-%m-%d") for x in temp]
        data['Date'] = temp2

        del temp,temp2
        data['Date'].values
        data
        cols=['Date','CO2 - PPM','CO - PPM','CH4 - PPM','Dust Particles (PM2.5)','Wind S','Wind D','Humidity']
        drop_column_for_PM25=['CO2 - PPM','CO - PPM','CH4 - PPM','Wind S','Wind D']
        data.drop(drop_column_for_PM25,axis=1,inplace=True)
        data=data.sort_values('Date')
        data
        send_build_status(1,15,'PENDING')
        data=data.sort_values('Date')
        data.isnull().sum()
        data = data.groupby('Date')['Dust Particles (PM2.5)'].sum().reset_index()
        data
        dust_particles=data['Dust Particles (PM2.5)']
        train_size = int(len(data) * 0.80)
        train_size
        train, test = data[0:train_size], data[train_size:len(data)]
        train
        train = train.set_index('Date')
        train.index
        y = train['Dust Particles (PM2.5)'].resample('MS').mean()
        y
        send_build_status(1,20,'PENDING')
        #Dust particles training Daily basis
        train.plot(figsize=(15, 6))
        # plt.show()
        #Average Dust particles Month basis
        y.plot(figsize=(15, 6))
        # plt.show()
        rcParams['figure.figsize'] = 18, 8
        decomposition = sm.tsa.seasonal_decompose(y, model='additive')
        fig = decomposition.plot()
        # plt.show()
        kpss_test(y)
        p = d = q = range(0, 2)
        pdq = list(itertools.product(p, d, q))
        seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]
        print('Examples of parameter combinations for Seasonal ARIMA...')
        print('SARIMAX: {} x {}'.format(pdq[1], seasonal_pdq[1]))
        print('SARIMAX: {} x {}'.format(pdq[1], seasonal_pdq[2]))
        print('SARIMAX: {} x {}'.format(pdq[2], seasonal_pdq[3]))
        print('SARIMAX: {} x {}'.format(pdq[2], seasonal_pdq[4]))
        send_build_status(1,30,'PENDING')
        pdqLength = len(pdq)
        nowPrecentage = 30
        oneSegnemtPrecentage = 50 / pdqLength
        for param in pdq:
            nowPrecentage = nowPrecentage + oneSegnemtPrecentage
            send_build_status(1,nowPrecentage + oneSegnemtPrecentage,'PENDING')
            for param_seasonal in seasonal_pdq:
                try:
                    mod = sm.tsa.statespace.SARIMAX(train,order=param,seasonal_order=param_seasonal,enforce_stationarity=False,enforce_invertibility=False)
                    results = mod.fit()
                    print('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal, results.aic))
                except:
                    continue
        #ARIMA(0, 0, 1)x(0, 1, 1, 12)12 - AIC,17537.985283942224
        # send_build_status(1,84,'PENDING')
        mod = sm.tsa.statespace.SARIMAX(train,order=(0, 0, 1),seasonal_order=(0, 1, 1, 12),enforce_stationarity=False,enforce_invertibility=False)
        model_fit= mod.fit()
        print(model_fit.summary().tables[1])
        send_build_status(1,98,'PENDING')
        print("going to save")
        joblib.dump(model_fit,'TS_model_for_Dust_particles.pkl')
        # model_fit.save('TS_model_for_Dust_particles.pkl')
        print("saved.....")
        send_build_status(1,100,'COMPLETED')
        # time.sleep(3)
        send_build_status(1,100,'DONE')
        # results.plot_diagnostics(figsize=(16, 8))
        # send_build_status(1,95,'PENDING')
        # # plt.show()
        # test
        # pred = results.get_prediction(start=pd.to_datetime('1995-01-01'), dynamic=False)
        # pred_ci = pred.conf_int()
        # ax = y['1990':].plot(label='observed')
        # pred.predicted_mean.plot(ax=ax, label='One-step ahead Forecast', alpha=.7, figsize=(14, 7))
        # ax.fill_between(pred_ci.index,pred_ci.iloc[:, 0],pred_ci.iloc[:, 1], color='k', alpha=.2)
        # ax.set_xlabel('Date')
        # ax.set_ylabel('Dust Particles (PM2.5)')
        # plt.legend()
        # y_forecasted = pred.predicted_mean
        # y_truth = train['1995-09-17':]
        # mse = ((y_forecasted - y_truth) ** 2).mean()
        # print('The Mean Squared Error of our forecasts is {}'.format(round(mse, 2)))
        # print('The Root Mean Squared Error of our forecasts is {}'.format(round(np.sqrt(mse), 2)))
        # pred_uc = results.get_forecast(steps=100)
        # pred_ci = pred_uc.conf_int()
        # ax = train.plot(label='observed', figsize=(14, 7))
        # pred_uc.predicted_mean.plot(ax=ax, label='Forecast')
        # ax.fill_between(pred_ci.index,pred_ci.iloc[:, 0],pred_ci.iloc[:, 1], color='k', alpha=.25)
        # ax.set_xlabel('Date')
        # ax.set_ylabel('Dust Particles (PM2.5)')
        # send_build_status(1,100,'COMPLETED')
        # plt.legend()
        # # plt.show()
        # model_loaded = AutoRegResults.load('TS_model_for_Dust_particles.pkl')
        # print(model_loaded.params)
        # test
        # model_loaded.predict('2021-05-07','2021-05-11')
    except:
        send_build_status(1,-1,'ERROR')        
# build_model()            
    