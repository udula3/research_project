from flask import Flask,render_template
from flask_pymongo import PyMongo
from flask_login import LoginManager
from flask_cors import CORS
from flask_socketio import SocketIO
import schedule
import time
import threading
import pyrebase
import json
import jsonify
from .firebase_object_factory import FirebaseObjectFactory
from .csv_data_factory import CsvDataFactory
app = Flask(__name__)
import csv
firebase_object_factory = FirebaseObjectFactory()
CORS(app)
print("CORS*************")
lm = LoginManager()
lm.init_app(app)
app.secret_key = "YOUR-SECRET-KEY"
UPLOAD_FOLDER = './'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config["MONGO_DBNAME"] = "ML_RESEARCH_AIR"
# app.config['MONGO_URI'] = 'mongodb://mongodb:27017/ML_RESEARCH_AIR'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/ML_RESEARCH_AIR'
app.mongo = PyMongo(app)
# app.socketio = SocketIO(app, cors_allowed_origins="*")
# app.socketio = SocketIO(app, cors_allowed_origins="*")
socketio = SocketIO(app, cors_allowed_origins="*")
app.APP_URL = "http://198.199.66.251:5000"


#adding Comment
        
# # schedule.every(0.01).minutes.do(job)
# while 1:
#     schedule.run_pending()
#     time.sleep(1)

def runJob():
    while True:
        for x in app.mongo.db.models.find():
            print(x)
            socketio.emit('rebuild', {'model': x['id'], 'status':x['status'],'progress':x['progress']})
            time.sleep(1)
            print("socket emited...")
print("job started...!")
thread = threading.Thread(target=runJob, args=())
thread.daemon = True
thread.start()
# app.socketio.emit('rebuild', {'model': 1, 'status':'PENDING','progress':nowPrecentage + oneSegnemtPrecentage})
@app.route("/")
def hello():
    return render_template('index.html')

@app.route("/test")
def test():
    gs1_firebase_db = firebase_object_factory.create_object('test')
    gs1_firebase_db.remove()
    todo= gs1_firebase_db.get()
    print(todo.val())
    print("*******************")
    return {"Status":True,"data":todo.val()}, 200
#
# with app.app_context():
#     print("Setting up indexes:")
#     eventIndex = app.mongo.db.collection.create_index([
#         ('title', "text"),
#     ],
#     name="collection_search_index",
#     weights={
#         'title':100,
#     })

if __name__ == "__main__":
    socketio.run(app,port=5000,host='0.0.0.0', )

from app import user_registration
from app import ml_model
from app import load_external_tada
