from app import app, lm
import numpy as np
from flask import jsonify
from flask import request
from datetime import datetime
import pickle
import regex
import threading
from threading import Thread
import time
from flask import Flask, request, jsonify
import model1_creation as mc
from sklearn.externals import joblib
import os.path
from os import path
import atexit
from statsmodels.tsa.ar_model import AutoRegResults
global ticket
dataLock = threading.Lock()
# thread handler
thread = threading.Thread()
cond = threading.Condition()
model = joblib.load('TS_model_for_Dust_particles.pkl') 

# POOL_TIME = 5 #Seconds
if path.exists("TS_model_for_Dust_particles.pkl") :
    model = joblib.load('TS_model_for_Dust_particles.pkl')
    print("No model now loaded in init")

# @app.route('/simple-get/', methods=['GET'])
# def simpleGet():
#   tableModel = mongo.db.tableName
#   output = []
#   for s in tableModel.find():
#     output.append({'id':s['id'],'NIC':s['NIC'],'name' : s['name']})
#   return jsonify(output)

# @app.route('/predict',methods=['POST'])
# def predict():
#     '''
#     For rendering results on HTML GUI
#     '''
#     int_features = [int(x) for x in request.form.values()]
#     final_features = [np.array(int_features)]
#     prediction = model.predict(final_features)

#     output = round(prediction[0], 2)

#     return render_template('index.html', prediction_text='Employee Salary should be $ {}'.format(output))
def interrupt():
        global thread
        thread.join()
        print('************************88')
        
def reload_model():
    global model
    model = joblib.load('TS_model_for_Dust_particles.pkl')
    print("No model now loaded in reload")
    return True
#prediction route
@app.route('/predict_api',methods=['POST'])
def predict_api():
    # try:
        print(path.exists("TS_model_for_Dust_particles.pkl"))
        print("model****")        
        if (path.exists("TS_model_for_Dust_particles.pkl")):
        # model = joblib.load('TS_model_for_Dust_particles.pkl',mmap_mode=None)
            data = request.get_json(force=True)
            val1 = validate_date(data['start_date'])
            val2 = validate_date(data['end_date'])
                
            if  val1 == True and val2 == True:
                print(data['start_date'])
                #call new model here.
                #[np.array(list(data.values()))] is input for example model
                print("going to Prdict")    
                prediction = model.predict(data['start_date'],data['end_date'])
                print("Prdicted")
                output = prediction
                print(output[0])
                return jsonify(output.to_json())
            else:
                return jsonify({"status":False,"error":"Date validation error"})
        else:
            return jsonify({"status":"Model not found! Please rebuild"})
            #  print("import model")
    # except:
            # return jsonify({"status":False,"error":"Somthing went wrong..!"})
#model build route. Model start to build with child thread and main thread return responce to user
# @app.route('/thread_start')
# def run_jobs():
#     thread = Thread(target=threaded_task)
#     thread.daemon = True
#     thread.start()
#     return jsonify({'thread_id': str(thread.id),
#                     'started': True})
@app.route('/stop_build')
def stop_build():
    result = app.mongo.db.models.update({"id":1},{'$set':{'progress':100,'status':"DONE"}})
    return jsonify({"status":True,"msg":"Stoped rebuilding!"})
@app.route('/rebuild/staus/<path:id>')
def rebuild_model_job_status(id):
    model_id = int(id)
    result = app.mongo.db.models.find_one({"id":model_id})
    print(result)
    if result == None:
        return jsonify({'status':False,"message":"Model not found"})
    else:
        return jsonify({'status':True,'model':model_id,'progress':result["progress"],'model_status':result["status"]})
@app.route('/build/model',methods=['POST'])
def run_model_creation_jobs():
    result = app.mongo.db.models.find_one({"id":1})
    if result == None:
        app.mongo.db.session.insert({"id" : 1,"progress" : 0,"status" : "PENDING"})
        # return jsonify({'status':True,'model':model_id,'progress':result["progress"],'model_status':result["status"]})
    thread = threading.Thread(target=mc.build_model, args=())
    thread.start()
    # atexit.register(interrupt)
    print(thread)
    return jsonify({'thread_name': str(thread.name),
                    'started': True})
#thread job. inside this we can call multi threaded long running task. ex - time.sleep()                    
def threaded_task():
    for i in range(5):
        print("Working... {}/{}".format(i + 1, 5))
        time.sleep(1)
    print("Completed... {}/{}".format(i + 1, 5))
def validate_date(date):
    try:
      valid_date = datetime.strptime(date, '%Y-%m-%d')
      print(valid_date)
      print('valid_date')
      if valid_date == None:
          return False
          print('Invalid date! in false')
      return True
    except ValueError:
      print('Invalid date! error')
      return False