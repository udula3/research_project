To run:

makesure `mognod` is running

then run `pip install -r reqs.txt` to install dependencys
last step run `python run.py`
now load `http://localhost:5000` to view UI


########################Request testing using POSTMAN####################################
#Test model using API
#URL
`http://localhost:5000/predict_api`

#BODY
`{"experience":2,"test_score":9, "interview_score":6}`

#Register User
`data = {"username": "myUser", "password":"myPass", "email": "d@d.com" }`
`post("http://127.0.0.1:5000/register", json=data).json()`

If succsessful then the json responce will be `{ 'success' : true, 'userId': '734891274389012', 'responce': "User saved"}`

#Login

`login = {"username":"myUser", "password": "myPass"}`
`post("http://127.0.0.1:5000/login", json=login).json()`

If succsessful then the json responce will be `{ 'success' : true, 'sessionId': '73333333334891274389012', 'responce': "User Logged In" } `
Save the sessionId for later use in your headers.


#Check to see if you can authenticate

Use base64 encoding for the sessionId when passing it in your headers. Add that header to access anything that is `@login_required`.

`headers={'Authorization': 'Basic ' + base64.b64encode(bytes('73333333334891274389012', 'utf-8')}
get("http://127.0.0.1:5000/write", headers=headers )`


`__init__.py` Contains the a stubbed out way of creating a index for searching mongoDB

Replace `YOUR-SECRET-KEY` in the `__init__.py` to whatever you want the secret to be.
Replace `YOUR_DB_NAME` in the `__init__.py` to whatever you want the secret to be.

