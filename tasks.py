import time

def threaded_task():
    for i in range(5):
        print("Working... {}/{}".format(i + 1, 5))
        time.sleep(1)
    print("Completed...!")